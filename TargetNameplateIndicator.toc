## Title: TargetNameplateIndicator
## Interface: 70300
## Notes: Puts a texture on your target's nameplate to make them stand out. Configure by editing the options in config.lua.
## Author: Choonster
## Version: 1.24
## OptionalDeps: LibNameplateRegistry-1.0
## X-Embeds: LibNameplateRegistry-1.0
## SavedVariables: TNIAddonDB

embeds.xml
config.lua
core.lua
