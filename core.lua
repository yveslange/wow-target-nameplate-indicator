------------------------------------------------------
-- Configuration variables have moved to config.lua --
--        Do not change anything in this file       --
------------------------------------------------------

-- List globals here for Mikk's FindGlobals script
-- GLOBALS: UnitIsUnit, UnitGUID, UnitIsFriend, CreateFrame, Mixin, print

hooksecurefunc("InterfaceOptionsFrame_OpenToCategory",InterfaceOptionsFrame_OpenToCategory)

local addon, ns = ...
local CONFIG = ns.CONFIG
-- local SELF, FRIENDLY, HOSTILE = CONFIG.SELF, CONFIG.FRIENDLY, CONFIG.HOSTILE

local TNI = CreateFrame("Frame", "TargetNameplateIndicator")
local LNR = LibStub("LibNameplateRegistry-1.0")
local LRC = LibStub("LibRangeCheck-2.0")
TNIAddon = LibStub("AceAddon-3.0"):NewAddon("TNIAddon", "AceConsole-3.0")

local textures = {
  NeonReticule = "Neon Reticule", 			 			-- Neon version of the reticule (contributed by mezmorizedck of Curse)
  NeonRedArrow = "Neon Red Arrow",						-- Neon version of the red arrow (contributed by mezmorizedck of Curse)
  RedChevronArrow = "Red Chevron Arrow",						-- Red inverted triple chevron (contributed by OligoFriends of Curse/WoWI)
  PaleRedChevronArrow = "Pale Red Chevron Arrow",				-- Pale red version of the chevron (contributed by OligoFriends of Curse/WoWI)
  arrow_tip_green = "Arrow Tip Green",						-- Green 3D arrow (contributed by OligoFriends of Curse/WoWI)
  arrow_tip_red = "Arrow Tip Red",						-- Red 3D arrow (contributed by OligoFriends of Curse/WoWI)
  skull = "Skull",										-- Skull and crossbones (contributed by OligoFriends of Curse/WoWI)
  circles_target = "Circles Target",						-- Red concentric circles in the style of a target (contributed by OligoFriends of Curse/WoWI)
  red_star = "Red Star",								-- Red star with gold outline (contributed by OligoFriends of Curse/WoWI)
  greenarrowtarget = "Green Arrow Target",				-- Neon green arrow with a red target (contributed by mezmorizedck of Curse)
  BlueArrow = "Blue Arrow",								-- Blue arrow pointing downwards (contributed by Imithat of WoWI)
  bluearrow1 = "Blue Arrow Abstract",								-- Abstract style blue arrow pointing downwards (contributed by Imithat of WoWI)
  gearsofwar = "GearsOfWar",								-- Gears of War logo (contributed by Imithat of WoWI)
  malthael = "Malthael",								-- Malthael (Diablo) logo (contributed by Imithat of WoWI)
  NewRedArrow = "New Red Arrow",								-- Red arrow pointing downwards, same style as BlueArrow (contributed by Imithat of WoWI)
  NewSkull = "New Skull",								-- Skull with gas mask (contributed by Imithat of WoWI)
  PurpleArrow = "Purple Arrow",								-- Abstract style purple arrow pointing downwards, same style as bluearrow1 (contributed by Imithat of WoWI)
  Shield = "Shield",										-- Kite shield with sword and crossed spears/polearms (contributed by Imithat of WoWI)
  NeonGreenArrow = "Neon Green Arrow",						-- Green version of the neon red arrow (contributed by Nokiya420 of Curse)
  Q_FelFlamingSkull = "Q FelFlamingSkull",				-- Fel green flaming skull (contributed by ContinuousQ of Curse)
  Q_RedFlamingSkull = "Q RedFlamingSkull",				-- Red flaming skull (contributed by ContinuousQ of Curse)
  Q_ShadowFlamingSkull = "Q ShadowFlamingSkull",		-- Shadow purple flaming skull (contributed by ContinuousQ of Curse)
  Q_GreenGPS = "Q GreenGPS",								-- Green map pin/GPS symbol (contributed by ContinuousQ of Curse)
  Q_RedGPS = "Q RedGPS",								-- Red map pin/GPS symbol (contributed by ContinuousQ of Curse)
  Q_WhiteGPS = "Q WhiteGPS",								-- White map pin/GPS symbol (contributed by ContinuousQ of Curse)
  Q_GreenTarget = "Q GreenTarget",						-- Green target arrow (contributed by ContinuousQ of Curse)
  Q_RedTarget = "Q RedTarget",								-- Red target arrow (contributed by ContinuousQ of Curse)
  Q_WhiteTarget = "Q WhiteTarget",						-- White target arrow (contributed by ContinuousQ of Curse)
  Hunters_Mark = "Hunters Mark",						-- Red Hunter's Mark Arrow (contributed by thisguyyouknow of Curse)
}

local defaults = {
	profile = {
		TARGET_ENABLED = true,
		HOSTILE_TEXTURE = "circles_target",
		MOUSEOVER_TEXTURE = "NeonRedArrow",
    MOUSEOVER = true,
		TEXTURE_HEIGHT = 50,
		TEXTURE_WIDTH = 50,
		MOUSEOVER_HEIGHT = 35,
		MOUSEOVER_WIDTH = 35,
		TEXTURE_POINT = "BOTTOM",    -- The point of the texture that should be anchored to the nameplate.
		ANCHOR_POINT  = "TOP",	   -- The point of the nameplate the texture should be anchored to.
		OFFSET_X = 0, 			   -- The x/y offset of the texture relative to the anchor point.
		OFFSET_Y = 5,
	},
}


local options = {
	name = "TNIAddon",
	handler = TNIAddon,
	type = 'group',
	args = {
		mainOptions = {
			order = 0,
			name = 'Main Options',
			type = 'group',
			handler = TNIAddon,
			args = {
				targetEnabled = {
					order = 0, 
					type = 'toggle',
					name = 'Enabled',
					descStyle = 'Enable the indicator on the selected target',
					get = 'GetTargetEnabledOption',
					set = 'SetTargetEnabledOption'
				},
				mouseover = {
					order = 1,
					type = 'toggle',
					name = 'Mouseover indicator',
					get = 'GetMouseOverOption',
					set = 'SetMouseOverOption',
					descStyle = "Should the mouseover indicator be displayed ?"
				},

			}
		},
		textures = {
			order = 1,
			name = 'Textures',
			type = 'group',
			handler = TNIAddon,
			args = {
				hTexture = {
					order = 0,
					type = 'select',
					values = textures,
					name = 'Main Texture',
					desc = 'The hostile texture',
					set = 'SetHostileTexture',
					get = 'GetHostileTexture'
				},
				mouseoverTexture = {
					order = 1,
					type = 'select',
					values = textures,
					name = 'Mouseover Texture',
					desc = 'The texture when mouse is over the target but not selected',
					set = 'SetMouseoverTexture',
					get = 'GetMouseoverTexture'
				},
			}
		},
		sizes = {
			order = 2,
			name = 'Size',
			type = 'group',
			handler = TNIAddon,
			args = {
				header1 = {
					order = 0,
					type = "header",
					name = "Indicator size of the selected target"
				},
				width = {
					order = 1,
					type = 'range',
					name = 'Width',
					min = 10,
					max = 100,
					desc = 'The width of the texture',
					set = 'SetWidthOption',
					get = 'GetWidthOption'
				},
				height = {
					order = 2,
					type = 'range',
					name = 'Height',
					min = 10,
					max = 100,
					desc = 'The height of the texture',
					set = 'SetHeightOption',
					get = 'GetHeightOption'
				},
				header2 = {
					order = 3,
					type = "header",
					name = "Size of the mouseover indicator"
				},
				mouseoverWidth = {
					order = 4,
					type = 'range',
					name = 'Mouseover width',
					min = 10,
					max = 100,
					desc = 'The width of the mouseover texture',
					set = 'SetMouseoverWidthOption',
					get = 'GetMouseoverWidthOption'
				},
				mouseoverHeight = {
					order = 5,
					type = 'range',
					name = 'Mouseover height',
					min = 10,
					max = 100,
					desc = 'The height of the mouseover texture',
					set = 'SetMouseoverHeightOption',
					get = 'GetMouseoverHeightOption'
				}
			}
		}
	},
}



function TNIAddon:OnInitialize()
  -- Code that you want to run when the addon is first loaded goes here.
	--self.db = LibStub("AceDB-3.0"):New("TNIAddonDB")
	self.db = LibStub("AceDB-3.0"):New("TNIAddonDB", defaults, true)
	--options.args.profile = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db)
	LibStub("AceConfig-3.0"):RegisterOptionsTable("TNIAddon", options)
	self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("TNIAddon", options.name)
	self:RegisterChatCommand("tni", "ChatCommand")
	if TNIAddon.db.profile.TARGET_ENABLED then
		InitTargetIndicator()
	end
  if TNIAddon.db.profile.MOUSEOVER then
    InitMouseOver()
  end
  -- TNIAddon:Print("|cff0033FFTNIinitilized")
end

function TNIAddon:ChatCommand(input)
	if not input or input:trim() == "" then
			InterfaceOptionsFrame_OpenToCategory(options.name)
	else
			LibStub("AceConfigCmd-3.0"):HandleCommand("tni", "TNIAddon", input)
	end
end

function TNIAddon:OnEnable()
  -- Called when the addon is enabled
  -- TNIAddon:Print("|cff33FF00TNIenabled");
end

function TNIAddon:OnDisable()
  -- Called when the addon is disabled
  -- TNIAddon:Print("|cffFF3333disabled")
end

function TNIAddon:GetHostileTexture(info)
  return self.db.profile.HOSTILE_TEXTURE
end

function TNIAddon:SetHostileTexture(info, input)
  self.db.profile.HOSTILE_TEXTURE = input
end

function TNIAddon:GetMouseOverOption(info)
  return self.db.profile.MOUSEOVER
end

function TNIAddon:SetMouseOverOption(info, input)
  self.db.profile.MOUSEOVER = input
  ReloadUI();
end

function TNIAddon:GetMouseoverTexture(info)
  return self.db.profile.MOUSEOVER_TEXTURE
end

function TNIAddon:SetMouseoverTexture(info, input)
  self.db.profile.MOUSEOVER_TEXTURE = input
end

function TNIAddon:GetTargetEnabledOption(info)
  return self.db.profile.TARGET_ENABLED
end

function TNIAddon:SetTargetEnabledOption(info, input)
  self.db.profile.TARGET_ENABLED = input
end

function TNIAddon:GetWidthOption(info)
  return self.db.profile.TEXTURE_WIDTH
end

function TNIAddon:SetWidthOption(info, input)
  self.db.profile.TEXTURE_WIDTH = input
end

function TNIAddon:GetHeightOption(info)
  return self.db.profile.TEXTURE_HEIGHT
end

function TNIAddon:SetHeightOption(info, input)
  self.db.profile.TEXTURE_HEIGHT = input
end

function TNIAddon:GetMouseoverWidthOption(info)
  return self.db.profile.MOUSEOVER_WIDTH
end

function TNIAddon:SetMouseoverWidthOption(info, input)
  self.db.profile.MOUSEOVER_WIDTH = input
end

function TNIAddon:GetMouseoverHeightOption(info)
  return self.db.profile.MOUSEOVER_HEIGHT
end

function TNIAddon:SetMouseoverHeightOption(info, input)
  self.db.profile.MOUSEOVER_HEIGHT = input
end

LNR:Embed(TNI)

--[===[@debug@
local DEBUG = true

local function debugprint(...)
	if DEBUG then
		print("TNI DEBUG:", ...)
	end
end

if DEBUG then
	TNI:LNR_RegisterCallback("LNR_DEBUG", debugprint)
end
--@end-debug@]===]

-----
-- Error callbacks
-----
local print, format = print, string.format

local function errorPrint(fatal, formatString, ...)
	local message = "|cffFF0000LibNameplateRegistry has encountered a" .. (fatal and " fatal" or "n") .. " error:|r"
	print("TargetNameplateIndicator:", message, format(formatString, ...))
end

function TNI:OnError_FatalIncompatibility(callback, incompatibilityType)
	local detailedMessage
	if incompatibilityType == "TRACKING: OnHide" or incompatibilityType == "TRACKING: OnShow" then
		detailedMessage = "LibNameplateRegistry missed several nameplate show and hide events."
	elseif incompatibilityType == "TRACKING: OnShow missed" then
		detailedMessage = "A nameplate was hidden but never shown."
	else
		detailedMessage = "Something has gone terribly wrong!"
	end

	errorPrint(true, "(Error Code: %s) %s", incompatibilityType, detailedMessage)
end

TNI:LNR_RegisterCallback("LNR_ERROR_FATAL_INCOMPATIBILITY", "OnError_FatalIncompatibility")

------
-- Nameplate callbacks
------
local Indicator = {}

function Indicator:Update(nameplate, isMouseover)
	self.currentNameplate = nameplate
	self.Texture:ClearAllPoints()

	--local config = UnitIsUnit("player", self.unit) and SELF or UnitIsFriend("player", self.unit) and FRIENDLY or HOSTILE

	if nameplate and TNIAddon.db.profile.TARGET_ENABLED then
		self.Texture:Show()
    if isMouseover then
			self.Texture:SetTexture("Interface\\AddOns\\TargetNameplateIndicator\\Textures\\" .. TNIAddon.db.profile.MOUSEOVER_TEXTURE)
			self.Texture:SetSize(TNIAddon.db.profile.MOUSEOVER_WIDTH, TNIAddon.db.profile.MOUSEOVER_HEIGHT)
    else
			self.Texture:SetTexture("Interface\\AddOns\\TargetNameplateIndicator\\Textures\\" .. TNIAddon.db.profile.HOSTILE_TEXTURE)
			self.Texture:SetSize(TNIAddon.db.profile.TEXTURE_WIDTH, TNIAddon.db.profile.TEXTURE_HEIGHT)
    end

		
		self.Texture:SetPoint(TNIAddon.db.profile.TEXTURE_POINT, nameplate, TNIAddon.db.profile.ANCHOR_POINT, TNIAddon.db.profile.OFFSET_X, TNIAddon.db.profile.OFFSET_Y)
	else
		self.Texture:Hide()
	end
end

function Indicator:OnRecyclePlate(callback, nameplate, plateData)
	--[===[@debug@
	debugprint("Callback fired (recycle)", self.unit, nameplate == self.currentNameplate)
	--@end-debug@]===]

	if nameplate == self.currentNameplate then
		self:Update()
	end
end

local function CreateIndicator(unit)
	local indicator = CreateFrame("Frame", "TargetNameplateIndicator_" .. unit)
	indicator.Texture = indicator:CreateTexture("$parentTexture", "OVERLAY")

	indicator.unit = unit

	LNR:Embed(indicator)
	Mixin(indicator, Indicator)

	indicator:LNR_RegisterCallback("LNR_ON_RECYCLE_PLATE", "OnRecyclePlate")

	indicator:SetScript("OnEvent", function(self, event, ...)
		self[event](self, ...)
	end)

	return indicator
end

------
-- Target Indicator
------

function InitTargetIndicator()
	local TargetIndicator = CreateIndicator("target")

	function TargetIndicator:PLAYER_TARGET_CHANGED()
		local nameplate, plateData = self:GetPlateByGUID(UnitGUID("target"))

		--[===[@debug@
		debugprint("Player target changed", nameplate)
		--@end-debug@]===]

		if not nameplate then
			self:Update()
		end
	end

	function TargetIndicator:OnTargetPlateOnScreen(callback, nameplate, plateData)
		--[===[@debug@
		debugprint("Callback fired (target found)")
		--@end-debug@]===]

		self:Update(nameplate)
	end

	TargetIndicator:RegisterEvent("PLAYER_TARGET_CHANGED")
	TargetIndicator:LNR_RegisterCallback("LNR_ON_TARGET_PLATE_ON_SCREEN", "OnTargetPlateOnScreen")
end

------
-- Mouseover Indicator
------

function InitMouseOver()
	local MouseoverIndicator = CreateIndicator("mouseover")

	function MouseoverIndicator:OnUpdate()
		-- If there's a current nameplate and it's still the mouseover unit, do nothing
		if self.currentNameplate and UnitIsUnit("mouseover", self.currentNameplate.namePlateUnitToken) then return end

		-- If there isn't a current nameplate and there's no mouseover unit, do nothing
		if not self.currentNameplate and not UnitExists("mouseover") then return end

		local nameplate, plateData = self:GetPlateByGUID(UnitGUID("mouseover"))

		local isMouseoverTarget = UnitIsUnit("mouseover", "target")

		--[===[@debug@
		debugprint("Player mouseover changed", nameplate, "isMouseoverTarget?", isMouseoverTarget)
		--@end-debug@]===]

		-- If the player has their mouse over a unit other than their target or the target indicator is disabled, update the mouseover indicator; otherwise hide it
		if not isMouseoverTarget or not TNIAddon.db.profile.TARGET_ENABLED then
			self:Update(nameplate, true)
		else
			self:Update(nil)
		end
	end

	MouseoverIndicator:SetScript("OnUpdate", MouseoverIndicator.OnUpdate)
end
